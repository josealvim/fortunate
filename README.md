# Installation

```sh
$ sudo make install
```

or just copy the script somewhere into `$PATH` (but you won't get the manpage that way).

# Requirements
* `python3` 
* `python3` modules:
    * `argparse`
    * `wikiquote`

# Usage
```
usage: fortunate [-h] [--list] [--opt n] [--all] query

Query Wikiquote

positional arguments:
  query       The query you are looking for. Expected to be a title or a name.

optional arguments:
  -h, --help  show this help message and exit
  --list      List results matching `query`.
  --opt n     Select n-th result matching `query`. Default = 0.
  --all       Display all quotes of selected match, as opposed to a random
              one.
```

Example:

```
$ fortunate "Richard Stallman" --all > rms
$ fortunate "Ted Kaczynski" | cowsay
 ________________________________________
/ A theme that appears repeatedly in the \
| writings of the social critics of the  |
| second half of the 20th century is the |
| sense of purposelessness that afflicts |
\ many people in modern society.         /
 ----------------------------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```
