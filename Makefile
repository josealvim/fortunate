ifeq ($(PREFIX),)
	PREFIX := /usr
endif

install :
	install -d $(DESTDIR)$(PREFIX)/bin
	install fortunate $(DESTDIR)$(PREFIX)/bin
	install -d $(DESTDIR)$(PREFIX)/local/share/man/man1
	install fortunate.1 $(DESTDIR)$(PREFIX)/local/share/man/man1
